FROM ruby:2.6.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
CMD bash -c "rake db:create db:migrate && rm -f tmp/pids/server.pid && bundle exec rails s -p 3001 -b '0.0.0.0'"
EXPOSE 3001
